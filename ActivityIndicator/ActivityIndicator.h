//
//  ActivityIndicator.h
//
//  Created by EvgenyMikhaylov on 9/1/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityIndicator : UIView

+ (void)ignoreInteractionEvents:(BOOL)ignore;
+ (void)setBackgroundColor:(UIColor*)color;
+ (void)setFont:(UIFont*)font;
+ (void)setSuccessImage:(UIImage*)image;
+ (void)setErrorImage:(UIImage*)image;
+ (void)setIndicatorColor:(UIColor*)color;
+ (void)setIndicatorSize:(CGSize)size;
+ (void)setIndicatorOffset:(UIOffset)offset;
+ (void)resetIndicatorOffset;
+ (void)setIndicatorCornerRadius:(CGFloat)radius;
+ (void)setIndicatorCircleRadius:(CGFloat)radius;
+ (void)setIndicatorCircleThickness:(CGFloat)thickness;
+ (void)setIndicatorCircleForegroundColor:(UIColor*)color;

+ (void)show;
+ (void)showInView:(UIView*)view;
+ (void)showWithProgress:(CGFloat)progress;
+ (void)showWithProgress:(CGFloat)progress inView:(UIView*)view;
+ (void)showWithProgress:(CGFloat)progress text:(NSString*)text;
+ (void)showWithProgress:(CGFloat)progress text:(NSString*)text inView:(UIView*)view;
+ (void)showWithText:(NSString*)text;
+ (void)showWithText:(NSString*)text inView:(UIView*)view;
+ (void)showSuccessWithText:(NSString*)text;
+ (void)showSuccessWithText:(NSString*)text inView:(UIView*)view;
+ (void)showErrorWithText:(NSString*)text;
+ (void)showErrorWithText:(NSString*)text inView:(UIView*)view;
+ (void)dismiss;
+ (void)dismissInView:(UIView*)view;
+ (void)dismissAfterDelay:(NSTimeInterval)delay;
+ (void)dismissAfterDelay:(NSTimeInterval)delay inView:(UIView*)view;

@end
