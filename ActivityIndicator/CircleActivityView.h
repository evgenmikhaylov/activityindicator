//
//  CircleActivityView.h
//
//  Created by EvgenyMikhaylov on 9/21/15.
//
//

#import <UIKit/UIKit.h>

@interface CircleActivityView : UIView

@property (nonatomic) CGFloat strokeThickness;
@property (nonatomic) CGFloat radius;
@property (nonatomic) UIColor *strokeColor;

- (void)startAnimating;
- (void)stopAnimating;

@end
