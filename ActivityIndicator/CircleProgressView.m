//
//  CircleProgressView.m
//
//  Created by EvgenyMikhaylov on 9/21/15.
//  Copyright © 2015 Rosberry. All rights reserved.
//

#import "CircleProgressView.h"

@interface CircleProgressView ()

@property (nonatomic) CAShapeLayer *backgroundLayer;
@property (nonatomic) CAShapeLayer *foregroundLayer;

@end

@implementation CircleProgressView

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    _progress = 0.0f;
    _strokeThickness = 2.0f;
    _radius = 20.0f;
    _foregroundStrokeColor = [UIColor blackColor];
    _backgroundStrokeColor = [UIColor grayColor];
    [self.layer addSublayer:self.backgroundLayer];
    [self.layer addSublayer:self.foregroundLayer];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    CGPoint center = CGPointMake(CGRectGetWidth(self.bounds)/2, CGRectGetHeight(self.bounds)/2);
    _backgroundLayer.frame = CGRectMake(center.x - _radius, center.y - _radius, _radius * 2, _radius * 2);
    _foregroundLayer.frame = CGRectMake(center.x - _radius, center.y - _radius, _radius * 2, _radius * 2);
}

#pragma mark - Setters

- (void)setProgress:(CGFloat)progress animated:(BOOL)animated{
    _progress = progress;
    if (animated) {
        _foregroundLayer.strokeEnd = progress;
    } else {
        [CATransaction begin];
        [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
        _foregroundLayer.strokeEnd = progress;
        [CATransaction commit];
    }
}

- (void)setStrokeThickness:(CGFloat)strokeThickness{
    _strokeThickness = strokeThickness;
    _backgroundLayer.lineWidth = _strokeThickness;
    _foregroundLayer.lineWidth = _strokeThickness;
}

- (void)setRadius:(CGFloat)radius{
    _radius = radius;
    _backgroundLayer.path = [self ringPath];
    _foregroundLayer.path = [self ringPath];
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)setForegroundStrokeColor:(UIColor *)foregroundStrokeColor{
    _foregroundStrokeColor = foregroundStrokeColor;
    _foregroundLayer.strokeColor = _foregroundStrokeColor.CGColor;
}

- (void)setBackgroundStrokeColor:(UIColor *)backgroundStrokeColor{
    _backgroundStrokeColor = backgroundStrokeColor;
    _backgroundLayer.strokeColor = _backgroundStrokeColor.CGColor;
}

#pragma mark - Getters

- (CAShapeLayer*)backgroundLayer{
    if(!_backgroundLayer){
        _backgroundLayer = [self progressLayer];
        _backgroundLayer.strokeEnd = 1.0f;
    }
    _backgroundLayer.strokeColor = self.backgroundStrokeColor.CGColor;
    _backgroundLayer.lineWidth = self.strokeThickness;
    _backgroundLayer.path = [self ringPath];
    return _backgroundLayer;
}

- (CAShapeLayer*)foregroundLayer{
    if(!_foregroundLayer){
        _foregroundLayer = [self progressLayer];
    }
    _foregroundLayer.strokeColor = self.foregroundStrokeColor.CGColor;
    _foregroundLayer.lineWidth = self.strokeThickness;
    _foregroundLayer.path = [self ringPath];
    _foregroundLayer.strokeEnd = self.progress;
    return _foregroundLayer;
}

- (CAShapeLayer*)progressLayer{
    CAShapeLayer *layer = [CAShapeLayer layer];
    layer.contentsScale = [[UIScreen mainScreen] scale];
    layer.fillColor = [UIColor clearColor].CGColor;
    layer.lineCap = kCALineCapRound;
    layer.lineJoin = kCALineJoinBevel;
    return layer;
}

- (CGPathRef)ringPath{
    UIBezierPath* path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(_radius, _radius)
                                                        radius:_radius
                                                    startAngle:(CGFloat)-M_PI_2
                                                      endAngle:(CGFloat)(M_PI + M_PI_2)
                                                     clockwise:YES];
    return path.CGPath;
}

@end