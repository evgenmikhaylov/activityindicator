//
//  CircleActivityView.m
//
//  Created by EvgenyMikhaylov on 9/21/15.
//
//

#import "CircleActivityView.h"

@interface CircleActivityView ()

@property (nonatomic) CAShapeLayer *circleActivityLayer;

@end

@implementation CircleActivityView

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.hidden = YES;
    _strokeThickness = 2.0f;
    _radius = 20.0f;
    _strokeColor = [UIColor blackColor];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    _circleActivityLayer.position = CGPointMake(CGRectGetWidth(self.bounds)/2, CGRectGetHeight(self.bounds)/2);
}

#pragma mark - Interface methods

- (void)startAnimating{
    self.hidden = NO;
    [self.layer addSublayer:self.circleActivityLayer];
}

- (void)stopAnimating{
    self.hidden = YES;
    [self.circleActivityLayer removeFromSuperlayer];
    self.circleActivityLayer = nil;
}

#pragma mark - Setters

- (void)setStrokeColor:(UIColor *)strokeColor {
    _strokeColor = strokeColor;
    _circleActivityLayer.strokeColor = strokeColor.CGColor;
}

- (void)setStrokeThickness:(CGFloat)strokeThickness {
    _strokeThickness = strokeThickness;
    _circleActivityLayer.lineWidth = _strokeThickness;
}

#pragma mark - Getters

- (CAShapeLayer*)circleActivityLayer {
    if(!_circleActivityLayer) {
        CGPoint arcCenter = CGPointMake(self.radius+self.strokeThickness/2+5, self.radius+self.strokeThickness/2+5);
        CGRect rect = CGRectMake(0.0f, 0.0f, arcCenter.x*2, arcCenter.y*2);
        
        UIBezierPath* smoothedPath = [UIBezierPath bezierPathWithArcCenter:arcCenter
                                                                    radius:self.radius
                                                                startAngle:(CGFloat) (M_PI*3/2)
                                                                  endAngle:(CGFloat) (M_PI/2+M_PI*5)
                                                                 clockwise:YES];
        
        _circleActivityLayer = [CAShapeLayer layer];
        _circleActivityLayer.contentsScale = [[UIScreen mainScreen] scale];
        _circleActivityLayer.frame = rect;
        _circleActivityLayer.fillColor = [UIColor clearColor].CGColor;
        _circleActivityLayer.strokeColor = self.strokeColor.CGColor;
        _circleActivityLayer.lineWidth = self.strokeThickness;
        _circleActivityLayer.lineCap = kCALineCapRound;
        _circleActivityLayer.lineJoin = kCALineJoinBevel;
        _circleActivityLayer.path = smoothedPath.CGPath;
        
        NSURL *url = [[NSBundle bundleForClass:self.class] URLForResource:@"ActivityIndicator" withExtension:@"bundle"];
        NSString *path = [[NSBundle bundleWithURL:url] pathForResource:@"angle-mask" ofType:@"png"];
        UIImage *maskImage = [UIImage imageWithContentsOfFile:path];
        CALayer *maskLayer = [CALayer layer];
        maskLayer.contents = (__bridge id)[maskImage CGImage];
        maskLayer.frame = _circleActivityLayer.bounds;
        _circleActivityLayer.mask = maskLayer;
        
        NSTimeInterval animationDuration = 1;
        CAMediaTimingFunction *linearCurve = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
        
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
        animation.fromValue = (id) 0;
        animation.toValue = @(M_PI*2);
        animation.duration = animationDuration;
        animation.timingFunction = linearCurve;
        animation.removedOnCompletion = NO;
        animation.repeatCount = INFINITY;
        animation.fillMode = kCAFillModeForwards;
        animation.autoreverses = NO;
        [_circleActivityLayer.mask addAnimation:animation forKey:@"rotate"];
        
        CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
        animationGroup.duration = animationDuration;
        animationGroup.repeatCount = INFINITY;
        animationGroup.removedOnCompletion = NO;
        animationGroup.timingFunction = linearCurve;
        
        CABasicAnimation *strokeStartAnimation = [CABasicAnimation animationWithKeyPath:@"strokeStart"];
        strokeStartAnimation.fromValue = @0.015;
        strokeStartAnimation.toValue = @0.515;
        
        CABasicAnimation *strokeEndAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
        strokeEndAnimation.fromValue = @0.485;
        strokeEndAnimation.toValue = @0.985;
        
        animationGroup.animations = @[strokeStartAnimation, strokeEndAnimation];
        [_circleActivityLayer addAnimation:animationGroup forKey:@"progress"];
    }
    return _circleActivityLayer;
}

@end
