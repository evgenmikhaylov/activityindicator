Pod::Spec.new do |s|

s.name                = "ActivityIndicator"
s.version             = "0.0.2"
s.summary             = "Custom activity indicator"
s.description         = <<-DESC
                        Custom activity indicator view
                        DESC
s.homepage            = "https://evgenmikhaylov@bitbucket.org/evgenmikhaylov/activityindicator"
s.license             = 'MIT'
s.author              = { "Evgeny Mikhaylov" => "evgenmikhaylov@gmail.com" }
s.source              = { :git => "https://evgenmikhaylov@bitbucket.org/evgenmikhaylov/activityindicator.git", :tag => "0.0.2" }
s.platform            = :ios, '8.0'
s.requires_arc        = true
s.source_files        = 'ActivityIndicator/*.{h,m}'
s.resources           = 'ActivityIndicator/ActivityIndicator.bundle'
s.dependency            'ArrayView'
end